const { Todo } = require('../models');
class todoController {
    static async findAll(req, res, next) {
        try {
            const data = await Todo.findAll();
            res.status(200).json(data);
        } catch (err) {
            next(err);
        }
    }
    static async findOne(req, res, next) {
        try {
            const data = await Todo.findOne({
                where: {
                    id: req.params.id
                }
            });
            if (data) {
                res.status(200).json(data);
            } else {
                throw { name: 'ErrorNotFound' }
            }
        } catch (err) {
            next(err);
        }
    }
    static async store(req, res, next) {
        try {
            console.log(req.body.title);
            const data = await Todo.create({
                title: req.body.title
            });
            res.status(200).json(data);
        } catch (err) {
            next(err);
        }
    }
    static async destroy(req, res, next) {
        try {
            const data = await Todo.destroy({
                where: {
                    id: req.params.id
                }
            });
            if (data === 1) {
                res.status(200).json({
                    message: 'Todo deleted successfully'
                });
            } else {
                throw { name: 'ErrorNotFound' }
            }
        } catch (err) {
            next(err);
        }
    }
}

module.exports = todoController;