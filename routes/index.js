const express = require('express');
const router = express.Router();
const todoController = require('../controllers/todoController.js');

router.get('/', todoController.findAll);
router.get('/:id', todoController.findOne);
router.post('/', todoController.store);
router.delete('/:id', todoController.destroy);

module.exports = router;