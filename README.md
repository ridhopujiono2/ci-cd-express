# Installation

`npm install`

# Migration

Do Migration First !

`sequelize db:create`

`sequelize db:migrate`

`sequelize db:seed:all`

# Testing

Change .env file

FROM

`NODE_ENV=development`

TO

`NODE_ENV=test`
