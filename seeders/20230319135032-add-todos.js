'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Todos', [
      {
        title: 'Leaning Python',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Leaning PHP',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Leaning Javascript',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Todos', null, {});
  }
};
