require("dotenv").config();
const express = require("express");
const app = express();
const port = 3000;
const router = require("./routes/index.js");
const errorHandler = require("./middleware/errorHandler.js");

app.use(express.json());
app.use(express.json());

app.use(router);
app.use(errorHandler);

if (process.env.NODE_ENV != "test") {
  app.listen(port, () => {
    console.log(`Server listening on ${port}`);
  });
}

module.exports = app;
