const request = require("supertest");
const app = require("../app");

describe("GET / [todo list]", () => {
  test("GET todo list", (done) => {
    request(app)
      .get("/")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        const firstData = response.body[0];
        expect(firstData.title).toBe("Leaning Python");
        done();
      })
      .catch(done);
  });
});
describe("GET /:id [todo by id]", () => {
  test("GET todo by id", (done) => {
    request(app)
      .get("/1")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        const firstData = response.body;
        expect(firstData.title).toBe("Leaning Python");
        done();
      })
      .catch(done);
  });
});
describe("POST / [store todo]", () => {
  test("STORE todo", (done) => {
    const newData = {
      title: "Learning Machine Learning",
    };
    request(app)
      .post("/")
      .send(newData)
      .expect("Content-Type", /json/)
      .then((response) => {
        expect(response.body.title).toBe("Learning Machine Learning");
        done();
      })
      .catch(done);
  });
});
describe("DELETE /:id [destroy todo]", () => {
  test("SOFT DELETE todo", (done) => {
    request(app)
      .delete("/4")
      .expect("Content-Type", /json/)
      .then((response) => {
        expect(response.body.message).toBe("Todo deleted successfully");
        done();
      })
      .catch(done);
  });
});
